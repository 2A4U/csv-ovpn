#!/usr/bin/env bash
getvpnlist () {
local vpnDir=${1}
local vpnDir=${vpnDir:-/dev/shm/vpn_cfg}
local serverList=cropped-list.txt
local tempList=vpn-list.txt

mkdir -p $vpnDir

wget --https-only --output-document=$tempList http://www.vpngate.net/api/iphone/

grep -v '^#\|^\*\|,-,' $tempList > $serverList

while read line ; do
readarray -t -d ',' subsection <<< ${line}
mkdir -p ${vpnDir}/${subsection[6],,}
echo -n "${subsection[14]}" | base64 --decode --ignore-garbage > ${vpnDir}/${subsection[6],,}/${subsection[0]}'.ovpn'
done < $serverList

echo " VPN lists saved to ${vpnDir}"

[[ -f $tempList ]] && rm $tempList $serverList
}
