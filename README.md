# CSV to OVPN
[csv-ovpn](https://gitlab.freedesktop.org/2A4U/csv-ovpn) — bash script to convert vpngate.net csv source to **.ovpn*.

## Getting Started
1. `git clone --depth 1 https://gitlab.freedesktop.org/2A4U/csv-ovpn`
2.  `source covpn.bash`
3. `getvpnlist` — writes **.ovpn* to /dev/shm/vpn_cfg

## How to Write to Custom Directory
```
getvpnlist mydirectory
```
